package scadufax.core.enums;

import java.util.Arrays;
import java.util.Optional;
import javax.ws.rs.core.Response.Status;

public enum StatusTransaction {
  ACCEPTED("aceito", "executado", Status.ACCEPTED),
  OK("sucesso", "executado com sucesso", Status.OK),
  CREATED("sucesso", "gravado com sucesso", Status.CREATED),
  NO_CONTENT("ok", "Sem mensagem adicional", Status.NO_CONTENT),
  NOT_FOUND("erro", "Não encontrado", Status.NOT_FOUND),
  BAD_REQUEST("erro", "erro ao executar", Status.BAD_REQUEST),
  UNAUTHORIZED(
    "autorizacao",
    "Usuario não autorizado. Boa tentativa...",
    Status.UNAUTHORIZED
  ),
  PRECONDITION_FAILED(
    "validacao",
    "requisição nula ou inválida",
    Status.PRECONDITION_FAILED
  ),
  FORBIDDEN("proibido", "Usuario não permitido.", Status.FORBIDDEN),
  INTERNAL_SERVER_ERROR("indefinido", "ambiguo", Status.INTERNAL_SERVER_ERROR);

  public String title;
  public String message;
  public Status status;

  private StatusTransaction(String title, String message, Status status) {
    this.title = title;
    this.message = message;
    this.status = status;
  }

  public static Optional<StatusTransaction> filter(Object obj) {
    if (obj instanceof Status) {
      return Arrays
        .stream(values())
        .filter(e -> e.status.equals(obj))
        .findFirst();
    }
    return Arrays
      .stream(values())
      .filter(e -> e.title.contentEquals(obj.toString()))
      .findFirst();
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
