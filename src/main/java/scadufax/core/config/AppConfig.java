package scadufax.core.config;

import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;

@OpenAPIDefinition(
  // servers = @Server(
  //   description = "Core de Servico",
  //   url = "{host}",
  //   variables = {
  //     @ServerVariable(
  //       name = "servidores",
  //       description = "Host da aplicacao",
  //       defaultValue = "http://localhost:8080",
  //       enumeration = {
  //         "http://localhost:8080",
  //       }
  //     ),
  //   }
  // ),
  info = @Info(
    title = "SCADUFAX-CORE",
    version = "1.0-SNAPSHOT",
    contact = @Contact(
      name = "Scadufax API Support",
      url = "http://scadufax.horse.com/contact",
      email = "scadufax@horse.com"
    )
  )
)
public class AppConfig extends Application {}
