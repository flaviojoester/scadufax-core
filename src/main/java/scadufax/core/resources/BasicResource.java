package scadufax.core.resources;

import java.util.HashMap;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.core.Response;

import scadufax.core.enums.StatusTransaction;
import scadufax.core.exceptions.ResourceServiceException;
import scadufax.core.models.dto.ErroNegocial;
import scadufax.core.models.dto.ResponseDTO;

public abstract class BasicResource<T> {
  @Inject
  Validator validator;

  public Response ok(Object object, String message) {
    ResponseDTO responseDTO = new ResponseDTO(
      message,
      StatusTransaction.OK,
      object
    );
    return Response
      .status(StatusTransaction.OK.status)
      .entity(responseDTO)
      .build();
  }

  public Response ok(Object object) {
    ResponseDTO responseDTO = new ResponseDTO(
      StatusTransaction.OK.message,
      StatusTransaction.OK,
      object
    );
    return Response
      .status(StatusTransaction.OK.status)
      .entity(responseDTO)
      .build();
  }

  public Response noContent() {
    return Response.status(StatusTransaction.NO_CONTENT.status).build();
  }

  public Response accepted(Object object) {
    ResponseDTO responseDTO = new ResponseDTO(
      StatusTransaction.ACCEPTED.message,
      StatusTransaction.ACCEPTED,
      object
    );
    return Response
      .status(StatusTransaction.ACCEPTED.status)
      .entity(responseDTO)
      .build();
  }

  public Response accepted(Object object, String message) {
    ResponseDTO responseDTO = new ResponseDTO(
      message,
      StatusTransaction.ACCEPTED,
      object
    );
    return Response
      .status(StatusTransaction.ACCEPTED.status)
      .entity(responseDTO)
      .build();
  }

  public Response created(Object object) {
    ResponseDTO responseDTO = new ResponseDTO(
      StatusTransaction.CREATED.message,
      StatusTransaction.CREATED,
      object
    );
    return Response
      .status(StatusTransaction.CREATED.status)
      .entity(responseDTO)
      .build();
  }

  public Response validate(T entity) throws ResourceServiceException {
    Set<ConstraintViolation<T>> violations = validator.validate(entity);

    if (violations != null && violations.size() > 0) {
      HashMap<String, Object> map = new HashMap<>();
      for (ConstraintViolation<?> v : violations) {
        map.put(
          v.getPropertyPath().toString().toLowerCase(),
          v.getMessage().toLowerCase()
        );
      }
      ErroNegocial erroNegocial = new ErroNegocial(
        StatusTransaction.BAD_REQUEST,
        StatusTransaction.BAD_REQUEST.message,
        map
      );

      throw new ResourceServiceException(erroNegocial);
    }
    return accepted(
      entity,
      String.format(
        "%s validado.",
        entity.getClass().getSimpleName().toUpperCase()
      )
    );
  }

  public void validate(String... obj) throws ResourceServiceException {
    ErroNegocial erroNegocial = new ErroNegocial(
      StatusTransaction.BAD_REQUEST,
      StatusTransaction.BAD_REQUEST.message,
      ""
    );
    for (String string : obj) {
      if (
        string == null || string.isEmpty()
      ) throw new ResourceServiceException(erroNegocial);
    }
  }
}
