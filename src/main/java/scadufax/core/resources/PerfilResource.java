package scadufax.core.resources;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.opentracing.Traced;

import io.quarkus.panache.common.Sort;
import scadufax.core.exceptions.ResourceServiceException;
import scadufax.core.exceptions.SuperException;
import scadufax.core.interfaces.PersistInterface;
import scadufax.core.models.dto.ErroNegocial;
import scadufax.core.models.dto.ResponseDTO;
import scadufax.core.models.persistence.Perfil;

@Traced
@Tag(name = "Perfil", description = "Perfis relacionados ao usuario")
@Path("/perfil")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class PerfilResource extends BasicResource<Perfil> {
  @Inject
  PersistInterface<Perfil> service;

  @GET
  @PermitAll
  @Operation(summary = "retorna todos os items")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "Para casos de sucesso", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ResponseDTO.class, type = SchemaType.DEFAULT))),
      @APIResponse(responseCode = "400", description = "Para casos de erro", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErroNegocial.class, type = SchemaType.DEFAULT))),
  })
  public Response get(@QueryParam("offset") @DefaultValue("0") int offset,
      @QueryParam("limit") @DefaultValue("10") int limit) throws ResourceServiceException {
    try {
      List<Perfil> entities = service.findAll(Sort.ascending("id"), offset, limit);
      return super.ok(entities);
    } catch (SuperException e) {
      throw new ResourceServiceException(e);
    }
  }

  @POST
  @PermitAll
  @Transactional
  @Operation(summary = "cadastra um novo item")
  @APIResponses(value = {
      @APIResponse(responseCode = "201", description = "Para casos de sucesso", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ResponseDTO.class, type = SchemaType.DEFAULT))),
      @APIResponse(responseCode = "400", description = "Para casos de erro", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErroNegocial.class, type = SchemaType.DEFAULT))),
  })
  public Response post(Perfil entity) throws ResourceServiceException {
    try {
      super.validate(entity);
      return super.created(service.insertOrUpdate(entity));
    } catch (SuperException e) {
      throw new ResourceServiceException(e);
    }
  }

  @PUT
  @PermitAll
  @Transactional
  @Operation(summary = "atualiza um item")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "Para casos de sucesso", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ResponseDTO.class, type = SchemaType.DEFAULT))),
      @APIResponse(responseCode = "400", description = "Para casos de erro", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErroNegocial.class, type = SchemaType.DEFAULT))),
      @APIResponse(responseCode = "404", description = "Para casos de erro", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErroNegocial.class, type = SchemaType.DEFAULT))),
  })
  public Response put(Perfil entity) throws ResourceServiceException {
    try {
      long id = entity.getId() != null ? entity.getId() : 0;
      super.validate(entity);
      Perfil updateEntity = service.findById(id);
      if (entity.getAdmin() != null)
        updateEntity.setAdmin(entity.getAdmin());
      if (entity.getDescription() != null)
        updateEntity.setDescription(
            entity.getDescription());
      if (entity.getTitle() != null)
        updateEntity.setTitle(entity.getTitle());
      return super.ok(service.insertOrUpdate(updateEntity));
    } catch (SuperException e) {
      throw new ResourceServiceException(e);
    }
  }

  @DELETE
  @Path("/{param}")
  @PermitAll
  @Transactional
  @Operation(summary = "deleta o item informado")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "Para casos de sucesso com informação adicional", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ResponseDTO.class, type = SchemaType.DEFAULT))),
      @APIResponse(responseCode = "204", description = "Para casos de sucesso", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
      @APIResponse(responseCode = "400", description = "Para casos de erro", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErroNegocial.class, type = SchemaType.DEFAULT))),
      @APIResponse(responseCode = "404", description = "Para casos de erro", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErroNegocial.class, type = SchemaType.DEFAULT))),
  })
  public Response delete(String param) throws ResourceServiceException {
    try {
      Perfil entity = service.find(param);
      validate(entity);
      service.delete(entity);
      return super.ok(param);
    } catch (SuperException e) {
      throw new ResourceServiceException(e);
    }
  }
}
