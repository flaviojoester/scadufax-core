package scadufax.core.impl;

import java.util.LinkedHashSet;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.jboss.logging.Logger;

import scadufax.core.enums.StatusTransaction;
import scadufax.core.exceptions.SuperException;
import scadufax.core.filters.SecurityRequestFilter;
import scadufax.core.interfaces.JwtInterface;
import scadufax.core.records.UserRecord;

@ApplicationScoped
public class JwtUserImpl implements JwtInterface {

  @Inject
  JsonWebToken jwt;

  private UserRecord userRecord;

  // @Initiate
  // public UserRecord getuser() {
  // FirebaseRecord firebase = jwt.getClaim("firebase") != null
  // ? jwt.getClaim("firebase")
  // : null;
  // }

  private static final Logger LOG = Logger.getLogger(
      SecurityRequestFilter.class);

  @Override
  public String toString() {
    return "userRecord=" + userRecord;
  }

  public JwtUserImpl() {
  }

  @Inject
  public JwtUserImpl(JsonWebToken jwt) {
    try {
      if (jwt == null) {
        throw new SuperException(StatusTransaction.UNAUTHORIZED, "Usuário não está logado");
      }

      LinkedHashSet<String> aud = jwt.getClaim(Claims.aud);
      Long authTime = jwt.getClaim(Claims.auth_time);
      boolean emailVerified = jwt.getClaim(Claims.email_verified);
      Long iat = jwt.getClaim(Claims.iat);
      Long exp = jwt.getClaim(Claims.exp);

      String iss = jwt.getClaim(Claims.iss);
      String sub = jwt.getClaim(Claims.sub);
      String email = jwt.getClaim(Claims.email);
      String name = jwt.getClaim("name") != null
          ? jwt.getClaim("name").toString()
          : null;
      String picture = jwt.getClaim("picture") != null
          ? jwt.getClaim("picture")
          : null;
      String userId = jwt.getClaim("user_id") != null
          ? jwt.getClaim("user_id")
          : null;

      userRecord = new UserRecord(userId, iss, sub, email, name, picture, aud, emailVerified, iat, exp, authTime);

      LOG.info("======> " + userRecord.toString());

    } catch (NullPointerException | ClassCastException e) {
      throw new SuperException(StatusTransaction.FORBIDDEN, e);
    }
  }

  @Override
  public UserRecord getUser() {
    return userRecord;
  }
}
