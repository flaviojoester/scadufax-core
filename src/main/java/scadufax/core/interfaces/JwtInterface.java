package scadufax.core.interfaces;

import scadufax.core.records.UserRecord;

public interface JwtInterface {
    public UserRecord getUser();
}
