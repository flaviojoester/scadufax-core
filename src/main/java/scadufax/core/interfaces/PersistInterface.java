package scadufax.core.interfaces;

import java.util.List;

import javax.enterprise.context.Dependent;

import io.quarkus.panache.common.Sort;
import scadufax.core.exceptions.ResourceServiceException;

@Dependent
public interface PersistInterface<T> {
  void delete(T entity) throws ResourceServiceException;

  T insertOrUpdate(T entity) throws ResourceServiceException;

  T find(String text) throws ResourceServiceException;

  T find(T entity) throws ResourceServiceException;

  T findById(Long id) throws ResourceServiceException;

  List<T> findAll(Sort ascending, int offset, int limit)
    throws ResourceServiceException;
}
