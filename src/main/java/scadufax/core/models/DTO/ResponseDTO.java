package scadufax.core.models.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import scadufax.core.enums.StatusTransaction;

@JsonInclude(Include.NON_NULL)
public class ResponseDTO implements Serializable {
  private static final long serialVersionUID = 1L;

  private String debugMessage;
  private StatusTransaction status;
  private Object object;

  public ResponseDTO(
    String debugMessage,
    StatusTransaction status,
    Object object
  ) {
    this.debugMessage = debugMessage;
    this.status = status;
    this.object = object;
  }

  public String getDebugMessage() {
    return debugMessage;
  }

  public void setDebugMessage(String debugMessage) {
    this.debugMessage = debugMessage;
  }

  public StatusTransaction getStatus() {
    return status;
  }

  public void setStatus(StatusTransaction status) {
    this.status = status;
  }

  public Object getObject() {
    return object;
  }

  public void setObject(Object object) {
    this.object = object;
  }

  @Override
  public String toString() {
    return (
      "UserDTO [debugMessage=" +
      debugMessage +
      ", status=" +
      status +
      ", object=" +
      object +
      "]"
    );
  }
}
