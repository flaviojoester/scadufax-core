package scadufax.core.models.persistence;

import java.time.LocalDateTime;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import scadufax.core.models.abstracted.BasicEntity;

@Entity
@Table(name = "tb_perfil")
public class Perfil extends BasicEntity {
  @Schema(title = "Titulo", example = "administrador")
  @NotBlank(message = "Não pode estar em branco")
  @Column(nullable = false, unique = true)
  private String title;

  private String description;

  @Column(nullable = false)
  private Boolean admin;

  @UpdateTimestamp
  @JsonFormat(
    shape = JsonFormat.Shape.STRING,
    pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ",
    locale = "pt_BR"
  )
  @JsonbTransient
  private LocalDateTime timestamp;

  @CreationTimestamp
  @JsonFormat(
    shape = JsonFormat.Shape.STRING,
    pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ",
    locale = "pt_BR"
  )
  @JsonbTransient
  @Column(nullable = false, updatable = false)
  private LocalDateTime createdTimestamp;

  @JsonIgnore
  @JsonbTransient
  @OneToMany(mappedBy = "perfil", cascade = CascadeType.ALL)
  private List<Horse> horses;

  public Perfil() {}

  public Perfil(String title) {
    this.title = title;
    this.admin = false;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getAdmin() {
    return admin;
  }

  public void setAdmin(Boolean admin) {
    this.admin = admin;
  }

  public List<Horse> getHorses() {
    return horses;
  }

  @Override
  public String toString() {
    return (
      "Perfil [title=" +
      title +
      ", description=" +
      description +
      ", admin=" +
      admin +
      ", timestamp=" +
      timestamp +
      ", createdTimestamp=" +
      createdTimestamp +
      ", horses=" +
      horses +
      "]"
    );
  }
}
