package scadufax.core.models.persistence;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import scadufax.core.models.abstracted.BasicEntity;

@Entity
@Table(name = "tb_note")
public class Note extends BasicEntity {
  @Schema(title = "Texto", example = "minhas notas")
  @NotBlank(message = "Não pode estar em branco")
  private String text;

  public Note() {}

  public Note(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return "Note [text=" + text + "]";
  }
}
