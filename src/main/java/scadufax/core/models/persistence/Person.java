package scadufax.core.models.persistence;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonFormat;

import scadufax.core.enums.Gender;
import scadufax.core.models.abstracted.BasicEntity;

@MappedSuperclass
public abstract class Person extends BasicEntity {
  @Schema(example = "João")
  @NotBlank(message = "não pode estar em branco")
  @Column(unique = false, nullable = false)
  private String name;

  @Schema(example = "Da Silva")
  private String middleName;

  @Schema(example = "Pereira")
  @NotBlank(message = "não pode estar em branco")
  @Column(unique = false, nullable = false)
  private String lastName;

  @Schema(title = "CPF, RG ou ID", example = "000.111.222-33")
  @NotBlank(message = "não pode estar em branco")
  private String identification;

  @JsonFormat(
    shape = JsonFormat.Shape.STRING,
    pattern = "yyyy-MM-dd",
    locale = "pt_BR"
  )
  @Schema(pattern = "yyyy-MM-dd", example = "1988-02-30")
  @NotBlank(message = "não pode estar em branco")
  private String birthdate;

  @Enumerated
  @Schema(example = "Masculino Ou Feminino")
  private Gender gender;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getIdentification() {
    return identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public void setGender(String gender) {
    this.gender = Gender.valueOf(gender.toUpperCase());
  }

  public String getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(String birthdate) {
    this.birthdate = birthdate;
  }

  @Override
  public String toString() {
    return (
      "Person [name=" +
      name +
      ", middleName=" +
      middleName +
      ", lastName=" +
      lastName +
      ", identification=" +
      identification +
      ", birthdate=" +
      birthdate +
      ", gender=" +
      gender +
      "]"
    );
  }
}
