package scadufax.core.models.persistence;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import scadufax.core.models.abstracted.BasicEntity;

@Entity
@Table(name = "tb_horse")
public class Horse extends BasicEntity {
  @Schema(title = "nome", example = "joão")
  @NotBlank(message = "Não pode estar em branco")
  @Column(unique = true, nullable = false)
  private String username;

  @Schema(title = "senha", example = "1234567@")
  @NotBlank(message = "Não pode estar em branco")
  @Column(nullable = false)
  private String password;

  @Valid
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "perfil_id", referencedColumnName = "id", nullable = true)
  private Perfil perfil;

  public Horse() {}

  public Horse(String username, String password, Perfil perfil) {
    this.username = username;
    this.password = password;
    this.perfil = perfil;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Perfil getPerfil() {
    return perfil;
  }

  public void setPerfil(Perfil perfil) {
    this.perfil = perfil;
  }

  @Override
  public String toString() {
    return (
      "Horse [username=" +
      username +
      ", password=" +
      password +
      ", perfil=" +
      perfil +
      "]"
    );
  }
}
