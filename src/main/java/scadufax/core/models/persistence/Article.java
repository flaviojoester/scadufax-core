package scadufax.core.models.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import scadufax.core.models.abstracted.BasicEntity;

@Entity
@Table(name = "tb_article")
public class Article extends BasicEntity {
  @Schema(title = "Titulo", example = "Titulo")
  @NotBlank(message = "Não pode estar em branco")
  @Column(nullable = false)
  private String title;

  @Schema(title = "Subtitulo do artigo", example = "Subtitulo do artigo")
  private String subtitle;

  @Schema(title = "Corpo do Artigo", example = "Corpo do Artigo")
  @Column(length = 5000)
  private String body;

  @Schema(title = "URL da imagem", example = "http://imagem.com")
  @NotBlank(message = "Não pode estar em branco")
  @Column(nullable = false)
  private String image;


  @Schema(title = "Rodapé do artigo", example = "Rodapé do artigo")
  private String footer;

  @Schema(title = "Link externo", example = "Link externo")
  private String link;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getFooter() {
    return footer;
  }

  public void setFooter(String footer) {
    this.footer = footer;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  @Override
  public String toString() {
    return (
      "Article [title=" +
      title +
      ", subtitle=" +
      subtitle +
      ", body=" +
      body +
      ", image=" +
      image +
      ", footer=" +
      footer +
      ", link=" +
      link +
      "]"
    );
  }
}
