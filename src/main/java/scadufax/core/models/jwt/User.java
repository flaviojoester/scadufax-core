package scadufax.core.models.jwt;

import java.util.ArrayList;
import java.util.List;

class User {
  private String username;

  public List<Role> roles = new ArrayList<>();

  public User() {}

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public List<Role> getRoles() {
    return roles;
  }

  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }

  @Override
  public String toString() {
    return "User [username=" + username + ", roles=" + roles + "]";
  }
}
