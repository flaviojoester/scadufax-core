package scadufax.core.models.jwt;

public class Role {
  private String role;

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  @Override
  public String toString() {
    return "Role [role=" + role + "]";
  }
}
