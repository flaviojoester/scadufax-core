package scadufax.core.filters;

import java.io.IOException;
import java.security.Principal;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.jwt.JsonWebToken;
import org.jboss.logging.Logger;

import scadufax.core.enums.StatusTransaction;
import scadufax.core.exceptions.SuperException;
import scadufax.core.impl.JwtUserImpl;

@Provider
public class SecurityRequestFilter implements ContainerRequestFilter {
        private static final Logger LOG = Logger.getLogger(
                        SecurityRequestFilter.class);

        @Inject
        JsonWebToken jwt;

        @Inject
        JwtUserImpl jwtService;

        @Override
        public void filter(ContainerRequestContext requestContext)
                        throws IOException {

                final SecurityContext securityContext = requestContext.getSecurityContext();

                // var principal = (DefaultJWTCallerPrincipal)
                // securityContext.getUserPrincipal();

                // if (
                // securityContext == null ||
                // !securityContext.isUserInRole("USER") &&
                // !securityContext.isUserInRole("ADMIN")
                // ) {
                // requestContext.abortWith(
                // Response
                // .status(Response.Status.UNAUTHORIZED)
                // .entity("Usuario não autorizado. Boa tentativa...")
                // .build()
                // );
                // } else

                if (securityContext == null || jwt == null || jwt.getName() == null
                                || securityContext.getUserPrincipal() == null
                                || !securityContext.getUserPrincipal().getName().equals(jwt.getName())) {
                        throw new SuperException(StatusTransaction.FORBIDDEN, "Não autorizado.");
                }

                greetings(securityContext);

        }

        protected Principal greetings(SecurityContext ctx) {
                Principal caller = ctx.getUserPrincipal();

                LOG.info("JWT: " + caller);

                String anonimous = caller == null ? "anonimo" : caller.getName();

                String msg = String.format(
                                "Oi %s, isHttps: %s, authScheme: %s, hasJWT: %s",
                                anonimous,
                                ctx.isSecure(),
                                ctx.getAuthenticationScheme(),
                                hasJwt());

                LOG.info(msg);

                return caller;
        }

        protected boolean hasJwt() {
                return jwt.getClaimNames() != null;
        }
}