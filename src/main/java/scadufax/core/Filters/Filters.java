package scadufax.core.filters;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;

import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.server.ServerRequestFilter;
import org.jboss.resteasy.reactive.server.ServerResponseFilter;


class Filters {
  private static final Logger LOG = Logger.getLogger(Filters.class);

  @ServerRequestFilter(preMatching = true)
  public void preMatchingFilter(ContainerRequestContext requestContext) {
    LOG.info("ServerRequestFilter: ");

    LOG.info(
      requestContext.getMethod() +
      " " +
      requestContext.getUriInfo().getAbsolutePath()
    );
    for (String key : requestContext.getHeaders().keySet()) {
      LOG.info(key + ": " + requestContext.getHeaders().get(key));
    }
  }

  @ServerResponseFilter
  public void getFilter(ContainerResponseContext responseContext) {
    Object entity = responseContext.getEntity();

    LOG.info("ServerResponseFilter: " + entity);

    for (String key : responseContext.getHeaders().keySet()) {
      LOG.info(key + ": " + responseContext.getHeaders().get(key));
    }
  }
  //   @ServerRequestFilter
  //   public Optional<RestResponse<Void>> getFilter(ContainerRequestContext ctx) {
  //     LOG.info("filtrousaida");
  //     // only allow GET methods for now
  //     if (ctx.getMethod().equals(HttpMethod.GET)) {
  //       return Optional.of(
  //         RestResponse.status(Response.Status.METHOD_NOT_ALLOWED)
  //       );
  //     }
  //     return Optional.empty();
  //   }
}
