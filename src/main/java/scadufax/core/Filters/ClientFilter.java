package scadufax.core.filters;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;

public class ClientFilter implements ClientRequestFilter, ClientResponseFilter {
  private final String secret;

  public ClientFilter(String secret) {
    this.secret = secret;
  }

  public void filter(ClientRequestContext req) {
    System.out.println("Configurando token request: " + secret);

    String base64Token = Base64
      .getEncoder()
      .encodeToString(secret.getBytes(StandardCharsets.UTF_8));

    req.getHeaders().add("Authorization", "Basic " + base64Token);
    System.out.println(
      "Added to HTTP Request Authorization [" + base64Token + "]"
    );
  }

  @Override
  public void filter(ClientRequestContext arg0, ClientResponseContext crc1)
    throws IOException {
    for (String key : crc1.getHeaders().keySet()) {
      System.out.println("Response Headers: " + crc1.getHeaders().get(key));
    }
  }
}
