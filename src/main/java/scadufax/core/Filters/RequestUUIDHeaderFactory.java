package scadufax.core.filters;

import java.util.Base64;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory;

@ApplicationScoped
public class RequestUUIDHeaderFactory implements ClientHeadersFactory {
  @ConfigProperty(name = "scadufax-jwt-api.user")
  protected String username;

  @ConfigProperty(name = "scadufax-jwt-api.secret")
  protected String secret;

  @Override
  public MultivaluedMap<String, String> update(
    MultivaluedMap<String, String> arg0,
    MultivaluedMap<String, String> arg1
  ) {
    MultivaluedMap<String, String> result = new MultivaluedHashMap<>();
    result.add("X-request-uuid", UUID.randomUUID().toString());
    result.add("Authorization", basic64Token());
    return result;
  }

  protected String basic64Token() {
    String token =
      "Basic " +
      Base64
        .getEncoder()
        .encodeToString(String.format("%s:%s", username, secret).getBytes());
    return token;
  }
}
