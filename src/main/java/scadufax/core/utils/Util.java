package scadufax.core.utils;

import io.smallrye.common.constraint.NotNull;

public class Util {

  public Util() {}

  public static String chainedString(@NotNull Throwable throwable) {
    StringBuilder SB = new StringBuilder(throwable.toString());
    while ((throwable = throwable.getCause()) != null) SB
      .append("\ncaused by ")
      .append(throwable);
    return SB.toString();
  }

  public static String chainedString(
    @NotNull String msg,
    @NotNull Throwable throwable
  ) {
    StringBuilder SB = new StringBuilder(msg);
    do {
      SB.append("\ncaused by ").append(throwable);
    } while ((throwable = throwable.getCause()) != null);
    return SB.toString();
  }
}
