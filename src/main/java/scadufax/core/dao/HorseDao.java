package scadufax.core.dao;

import javax.enterprise.context.RequestScoped;
import javax.persistence.PersistenceException;

import org.eclipse.microprofile.opentracing.Traced;
import org.hibernate.exception.ConstraintViolationException;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import scadufax.core.models.persistence.Horse;

@Traced
@RequestScoped
public class HorseDao implements PanacheRepository<Horse> {

  public Horse insertOrUpdate(Horse entity) throws PersistenceException {
    try {
      if (!isPersistent(entity)) {
        persistAndFlush(entity);
      }
      return entity;
    } catch (ConstraintViolationException e) {
      throw new PersistenceException("Existente");
    }
  }
}
