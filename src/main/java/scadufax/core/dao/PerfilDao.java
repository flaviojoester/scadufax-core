package scadufax.core.dao;

import javax.enterprise.context.RequestScoped;
import javax.persistence.PersistenceException;

import org.eclipse.microprofile.opentracing.Traced;
import org.hibernate.exception.ConstraintViolationException;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import scadufax.core.models.persistence.Perfil;

@Traced
@RequestScoped
public class PerfilDao implements PanacheRepository<Perfil> {

  public Perfil insertOrUpdate(Perfil entity) throws PersistenceException {
    try {
      if (!isPersistent(entity)) {
        persistAndFlush(entity);
      }
      return entity;
    } catch (ConstraintViolationException e) {
      throw new PersistenceException("Existente");
    }
  }
}
