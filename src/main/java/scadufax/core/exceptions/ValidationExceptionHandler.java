package scadufax.core.exceptions;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path.Node;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import scadufax.core.enums.StatusTransaction;
import scadufax.core.models.dto.ErroNegocial;

@Provider
public class ValidationExceptionHandler implements ExceptionMapper<ConstraintViolationException> {

    private static final Logger LOG = Logger.getLogger(ValidationExceptionHandler.class);

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        String message = exception.getCause() != null &&
                exception.getCause().getMessage() != null
                        ? exception.getCause().getMessage()
                        : exception.getMessage();

        Map<String, String> violations = getViolations(exception.getConstraintViolations());

        ErroNegocial erro = new ErroNegocial(
                StatusTransaction.PRECONDITION_FAILED,
                message,
                violations.toString());

        LOG.error(erro, exception);

        return Response.status(erro.getStatus().status).entity(erro).build();
    }

    private Map<String, String> getViolations(Set<ConstraintViolation<?>> set) {

        LinkedHashMap<String, String> map = new LinkedHashMap<>();

        for (ConstraintViolation<?> v : set) {
            String name = null;
            for (Node node : v.getPropertyPath()) {
                name = node.getName();
            }
            map.put(name, v.getMessage());
        }

        return map;
    }

}
