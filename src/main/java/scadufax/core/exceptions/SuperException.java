package scadufax.core.exceptions;

import scadufax.core.enums.StatusTransaction;

public class SuperException extends RuntimeException {
  private StatusTransaction statusTransaction = StatusTransaction.PRECONDITION_FAILED;

  public SuperException(String message) {
    super(message);
  }

  public SuperException(String message, Exception exception) {
    super(message, exception);
  }

  public SuperException(Exception exception) {
    super(exception);
  }

  public SuperException(StatusTransaction statusTransaction) {
    super(statusTransaction.message);
    this.statusTransaction = statusTransaction;
  }

  public SuperException(StatusTransaction statusTransaction, String message) {
    super(message);
    this.statusTransaction = statusTransaction;
  }

  public SuperException(StatusTransaction statusTransaction, Exception e) {
    super(e);
    this.statusTransaction = statusTransaction;
  }

  public StatusTransaction getStatusTransaction() {
    return statusTransaction;
  }

}
