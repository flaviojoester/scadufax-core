package scadufax.core.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.hibernate.exception.ConstraintViolationException;
import org.jboss.logging.Logger;

import scadufax.core.enums.StatusTransaction;
import scadufax.core.models.dto.ErroNegocial;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {
  private static final Logger LOG = Logger.getLogger(
      ExceptionHandler.class);

  @Override
  public Response toResponse(Exception exception) {
    String message = exception.getCause() != null &&
        exception.getCause().getMessage() != null
            ? exception.getCause().getMessage()
            : exception.getMessage();

    ErroNegocial erro = new ErroNegocial(
        StatusTransaction.INTERNAL_SERVER_ERROR,
        message,
        exception.getMessage());

    if (exception instanceof ResourceServiceException) {
      ResourceServiceException resourceException = (ResourceServiceException) exception;
      if (resourceException.getErroNegocial() != null) {
        erro = resourceException.getErroNegocial();
      }
    } else if (exception instanceof ConstraintViolationException) {
      erro.setStatus(StatusTransaction.BAD_REQUEST);
      erro.setDebugMessage(
          ((ConstraintViolationException) exception).getConstraintName());
    } else if (exception instanceof SuperException) {
      SuperException e = (SuperException) exception;
      if (e.getStatusTransaction() != null) {
        erro = new ErroNegocial(
            e.getStatusTransaction(),
            message,
            exception.getMessage());
      }
    }

    LOG.error(erro, exception);

    return Response.status(erro.getStatus().status).entity(erro).build();
  }
}
