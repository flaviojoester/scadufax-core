package scadufax.core.records;

import java.io.Serializable;
import java.util.LinkedHashSet;

public final record UserRecord(
        String userId,
        String iss,
        String sub,
        String email,
        String name,
        String picture,
        LinkedHashSet<String> aud,
        Boolean emailVerified,
        Long iat,
        Long exp, Long authTime) implements Serializable {
}
