package scadufax.core.records;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public final record FirebaseRecord(
        @JsonProperty(value = "sign_in_provider") String signInProvider)
        implements Serializable {
}
