package scadufax.core.records;

public record IdentitiesRecord(String email) {
}