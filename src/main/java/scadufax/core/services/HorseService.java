package scadufax.core.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import io.quarkus.panache.common.Sort;
import scadufax.core.dao.HorseDao;
import scadufax.core.enums.StatusTransaction;
import scadufax.core.exceptions.ResourceServiceException;
import scadufax.core.interfaces.PersistInterface;
import scadufax.core.models.dto.ErroNegocial;
import scadufax.core.models.persistence.Horse;

@ApplicationScoped
public class HorseService implements PersistInterface<Horse> {
  @Inject
  HorseDao dao;

  ErroNegocial erroNegocial = new ErroNegocial(
      StatusTransaction.PRECONDITION_FAILED,
      "Erro Generico",
      this.getClass());

  @Override
  public void delete(Horse entity) throws ResourceServiceException {
    try {
      dao.delete(entity);
    } catch (PersistenceException e) {
      erroNegocial.setDebugMessage(
          String.format("%s: %s", getClass().getSimpleName(), e.getMessage()));
      throw new ResourceServiceException(erroNegocial);
    }
  }

  @Override
  public Horse insertOrUpdate(Horse entity) throws ResourceServiceException {
    try {
      return dao.insertOrUpdate(entity);
    } catch (PersistenceException e) {
      erroNegocial.setDebugMessage(
          String.format("%s: %s", getClass().getSimpleName(), e.getMessage()));
      throw new ResourceServiceException(erroNegocial);
    }
  }

  @Override
  public Horse find(String text) throws ResourceServiceException {
    try {
      return dao
          .find("username", text)
          .firstResultOptional()
          .orElseThrow(() -> new PersistenceException("Não encontrado."));
    } catch (PersistenceException e) {
      erroNegocial.setDebugMessage(
          String.format("%s: %s", getClass().getSimpleName(), e.getMessage()));
      throw new ResourceServiceException(erroNegocial);
    }
  }

  @Override
  public Horse findById(Long id) throws ResourceServiceException {
    try {
      return dao
          .findByIdOptional(id)
          .orElseThrow(
              () -> new ResourceServiceException(
                  new ErroNegocial(
                      StatusTransaction.NOT_FOUND,
                      "Não encontrado",
                      "Identificação não encontrado")));
    } catch (PersistenceException e) {
      erroNegocial.setDebugMessage(
          String.format("%s: %s", getClass().getSimpleName(), e.getMessage()));
      throw new ResourceServiceException(erroNegocial);
    }
  }

  @Override
  public List<Horse> findAll(Sort ascending, int offset, int limit) throws ResourceServiceException {
    try {
      return dao.findAll(ascending).page(offset, limit).list();
    } catch (PersistenceException e) {
      erroNegocial.setDebugMessage(
          String.format("%s: %s", getClass().getSimpleName(), e.getMessage()));
      throw new ResourceServiceException(erroNegocial);
    }
  }

  // @Override
  public Horse findByToken(String token) throws ResourceServiceException {
    try {
      return dao
          .find("token", token)
          .firstResultOptional()
          .orElseThrow(() -> new PersistenceException("Não encontrado."));
    } catch (PersistenceException e) {
      erroNegocial.setDebugMessage(
          String.format("%s: %s", getClass().getSimpleName(), e.getMessage()));
      throw new ResourceServiceException(erroNegocial);
    }
  }

  @Override
  public Horse find(Horse entity) throws ResourceServiceException {
    return find(entity.getUsername());
  }
}
