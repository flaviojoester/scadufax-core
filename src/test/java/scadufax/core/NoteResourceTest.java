package scadufax.core;

import static io.restassured.RestAssured.given;

import org.hamcrest.core.IsAnything;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.JwtSecurity;

@QuarkusTest
public class NoteResourceTest {

  @Test
  @TestSecurity(user = "teste", roles = { "ADMIN", "USER" })
  @JwtSecurity(claims = {
      @Claim(key = "iss", value = "https://securetoken.google.com/scadufax-6c2d5"),
      @Claim(key = "auth_time", value = "123123"),
      @Claim(key = "aud", value = "[scadufax-6c2d5]"),
      @Claim(key = "sub", value = "YHnQ91fIpUhQYfi5BmfFXhYLBzK2"),
      @Claim(key = "email", value = "teste@gmail.com"),
      @Claim(key = "email_verified", value = "true"),
      @Claim(key = "iat", value = "1693161435"),
      @Claim(key = "exp", value = "1693161435"),
      @Claim(key = "name", value = "YHnQ91fIpUhQYfi5BmfFXhYLBzK2"),
      @Claim(key = "picture", value = "https://lh3.googleusercontent.com/a/AEdFTp4qR7GJdxnw1aEZVSUWAh5o4g39zTWK17ABZYAOVlI=s96-c"),
      @Claim(key = "user_id", value = "YHnQ91fIpUhQYfi5BmfFXhYLBzK2"),
  })
  public void get() {
    given()
        .when()
        .get("/note")
        .then()
        .statusCode(200)
        .body(IsAnything.anything());
  }
}
