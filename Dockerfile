FROM maven:3.8.3-openjdk-17 as BUILD

COPY . /usr/src/app
RUN mvn --batch-mode -f /usr/src/app/pom.xml clean package

FROM openjdk:17-oracle

COPY --from=BUILD /usr/src/app/target/quarkus-app/ /opt/
COPY --from=BUILD /usr/src/app/target/quarkus-app/*.jar /opt/app.jar

EXPOSE 8080
EXPOSE 80
WORKDIR /opt

ENTRYPOINT ["java", "-Duser.country=BR", "-Duser.language=pt", "-jar", "/opt/app.jar"]